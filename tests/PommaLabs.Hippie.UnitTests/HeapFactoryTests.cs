﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests;

[TestFixture]
public class HeapFactoryTests
{
    [Test]
    public void NewBinomialHeap_IntHeap_WithCustomPriorityComparer()
    {
        var heap = HeapFactory.NewBinomialHeap<string, int>(Comparer<int>.Default);
        AssertHeapCorrectness(heap, Comparer<int>.Default, EqualityComparer<string>.Default);
    }

    [Test]
    public void NewBinomialHeap_IntHeap_WithDefaultPriorityComparer()
    {
        var heap = HeapFactory.NewBinomialHeap<string, int>();
        AssertHeapCorrectness(heap, BetterComparer<int>.Default, EqualityComparer<string>.Default);
    }

    [Test]
    public void NewBinomialHeap_StringHeap_WithCustomPriorityComparer()
    {
        var heap = HeapFactory.NewBinomialHeap<string, string>(Comparer<string>.Default);
        AssertHeapCorrectness(heap, Comparer<string>.Default, EqualityComparer<string>.Default);
    }

    [Test]
    public void NewBinomialHeap_StringHeap_WithDefaultPriorityComparer()
    {
        var heap = HeapFactory.NewBinomialHeap<string, string>();
        AssertHeapCorrectness(heap, BetterComparer<string>.Default, EqualityComparer<string>.Default);
    }

    [Test]
    public void NewMultiArrayHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => HeapFactory.NewArrayHeap(7, null, EqualityComparer<A>.Default));
    }

    [Test]
    public void NewMultiBinaryHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewMultiHeap_NullPriorityComparer(HeapFactory.NewBinaryHeap));
    }

    [Test]
    public void NewMultiBinaryIntHeap_CheckDefaultPriorityComparer()
    {
        NewMultiIntHeap_CheckDefaultComparer(HeapFactory.NewBinaryHeap);
    }

    [Test]
    public void NewMultiBinomialHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewMultiHeap_NullPriorityComparer(HeapFactory.NewBinomialHeap));
    }

    [Test]
    public void NewMultiBinomialIntHeap_CheckDefaultPriorityComparer()
    {
        NewMultiIntHeap_CheckDefaultComparer(HeapFactory.NewBinomialHeap);
    }

    [Test]
    public void NewMultiFibonacciHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewMultiHeap_NullPriorityComparer(HeapFactory.NewFibonacciHeap));
    }

    [Test]
    public void NewMultiFibonacciIntHeap_CheckDefaultPriorityComparer()
    {
        NewMultiIntHeap_CheckDefaultComparer(HeapFactory.NewFibonacciHeap);
    }

    [Test]
    public void NewMultiPairingHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewMultiHeap_NullPriorityComparer(HeapFactory.NewPairingHeap));
    }

    [Test]
    public void NewMultiPairingIntHeap_CheckDefaultPriorityComparer()
    {
        NewMultiIntHeap_CheckDefaultComparer(HeapFactory.NewPairingHeap);
    }

    [Test]
    public void NewRawArrayHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => HeapFactory.NewRawArrayHeap<A, B>(7, null));
    }

    [Test]
    public void NewRawArrayHeap_OneChildCount()
    {
        Should.Throw<ArgumentOutOfRangeException>(() => HeapFactory.NewRawArrayHeap<A, int>(1));
    }

    [Test]
    public void NewRawArrayHeap_ZeroChildCount()
    {
        Should.Throw<ArgumentOutOfRangeException>(() => HeapFactory.NewRawArrayHeap<A, int>(0));
    }

    [Test]
    public void NewRawBinaryHeap_IntHeap_WithCustomPriorityComparer()
    {
        var heap = HeapFactory.NewRawBinaryHeap<int, int>(Comparer<int>.Default);
        AssertHeapCorrectness(heap, Comparer<int>.Default);
    }

    [Test]
    public void NewRawBinaryHeap_IntHeap_WithDefaultPriorityComparer()
    {
        var heap = HeapFactory.NewRawBinaryHeap<int, int>();
        AssertHeapCorrectness(heap, BetterComparer<int>.Default);
    }

    [Test]
    public void NewRawBinaryHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewRawHeap_NullPriorityComparer(HeapFactory.NewRawBinaryHeap<A, B>));
    }

    [Test]
    public void NewRawBinaryHeap_StringHeap_WithCustomPriorityComparer()
    {
        var heap = HeapFactory.NewRawBinaryHeap<string, string>(Comparer<string>.Default);
        AssertHeapCorrectness(heap, Comparer<string>.Default);
    }

    [Test]
    public void NewRawBinaryHeap_StringHeap_WithDefaultPriorityComparer()
    {
        var heap = HeapFactory.NewRawBinaryHeap<string, string>();
        AssertHeapCorrectness(heap, BetterComparer<string>.Default);
    }

    [Test]
    public void NewRawBinaryIntHeap_CheckDefaultPriorityComparer()
    {
        NewRawIntHeap_CheckDefaultComparer(HeapFactory.NewRawBinaryHeap<A, int>);
    }

    [Test]
    public void NewRawBinomialHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewRawHeap_NullPriorityComparer(HeapFactory.NewRawBinomialHeap<A, B>));
    }

    [Test]
    public void NewRawBinomialIntHeap_CheckDefaultPriorityComparer()
    {
        NewRawIntHeap_CheckDefaultComparer(HeapFactory.NewRawBinomialHeap<A, int>);
    }

    [Test]
    public void NewRawFibonacciHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewRawHeap_NullPriorityComparer(HeapFactory.NewRawFibonacciHeap<A, B>));
    }

    [Test]
    public void NewRawFibonacciIntHeap_CheckDefaultPriorityComparer()
    {
        NewRawIntHeap_CheckDefaultComparer(HeapFactory.NewRawFibonacciHeap<A, int>);
    }

    [Test]
    public void NewRawPairingHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewRawHeap_NullPriorityComparer(HeapFactory.NewRawPairingHeap<A, B>));
    }

    [Test]
    public void NewRawPairingIntHeap_CheckDefaultPriorityComparer()
    {
        NewRawIntHeap_CheckDefaultComparer(HeapFactory.NewRawPairingHeap<A, int>);
    }

    [Test]
    public void NewUniqueArrayHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => HeapFactory.NewArrayHeap<A, B>(7, null, EqualityComparer<A>.Default));
    }

    [Test]
    public void NewUniqueBinaryHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewUniqueHeap_NullPriorityComparer(HeapFactory.NewBinaryHeap));
    }

    [Test]
    public void NewUniqueBinaryIntHeap_CheckDefaultPriorityComparer()
    {
        NewUniqueIntHeap_CheckDefaultComparer(HeapFactory.NewBinaryHeap<A, int>);
    }

    [Test]
    public void NewUniqueBinomialHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewUniqueHeap_NullPriorityComparer(HeapFactory.NewBinomialHeap));
    }

    [Test]
    public void NewUniqueBinomialIntHeap_CheckDefaultPriorityComparer()
    {
        NewUniqueIntHeap_CheckDefaultComparer(HeapFactory.NewBinaryHeap<A, int>);
    }

    [Test]
    public void NewUniqueFibonacciHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewUniqueHeap_NullPriorityComparer(HeapFactory.NewFibonacciHeap));
    }

    [Test]
    public void NewUniqueFibonacciIntHeap_CheckDefaultPriorityComparer()
    {
        NewUniqueIntHeap_CheckDefaultComparer(HeapFactory.NewFibonacciHeap<A, int>);
    }

    [Test]
    public void NewUniquePairingHeap_NullPriorityComparer()
    {
        Should.Throw<ArgumentNullException>(() => NewUniqueHeap_NullPriorityComparer(HeapFactory.NewPairingHeap));
    }

    [Test]
    public void NewUniquePairingIntHeap_CheckDefaultPriorityComparer()
    {
        NewUniqueIntHeap_CheckDefaultComparer(HeapFactory.NewPairingHeap<A, int>);
    }

    private static void AssertHeapCorrectness<TVal, TPr>(IRawHeap<TVal, TPr> heap, IComparer<TPr> cmp)
    {
        Assert.That(heap, Is.Not.Null);
        Assert.That(heap.Comparer, Is.EqualTo(cmp));
        Assert.That(heap, Is.Empty);
    }

    private static void AssertHeapCorrectness<TVal, TPr>(IHeap<TVal, TPr> heap, IComparer<TPr> cmp, IEqualityComparer<TVal> eqCmp)
    {
        Assert.That(heap, Is.Not.Null);
        Assert.That(heap.Comparer, Is.EqualTo(cmp));
        Assert.That(heap.EqualityComparer, Is.EqualTo(eqCmp));
        Assert.That(heap, Is.Empty);
    }

    private static void AssertHeapCorrectness<TItem>(IHeap<TItem> heap, IComparer<TItem> cmp, IEqualityComparer<TItem> eqCmp)
    {
        Assert.That(heap, Is.Not.Null);
        Assert.That(heap.Comparer, Is.EqualTo(cmp));
        Assert.That(heap.EqualityComparer, Is.EqualTo(eqCmp));
        Assert.That(heap, Is.Empty);
    }

    private static void NewMultiHeap_NullPriorityComparer(Func<IComparer<A>, IEqualityComparer<A>, IHeap<A>> ctor)
    {
        ctor(null, EqualityComparer<A>.Default);
    }

    private static void NewMultiIntHeap_CheckDefaultComparer(Func<IEqualityComparer<int>, IHeap<int>> ctor)
    {
        var heap = ctor(EqualityComparer<int>.Default);
        AssertHeapCorrectness(heap, BetterComparer<int>.Default, EqualityComparer<int>.Default);
    }

    private static void NewRawHeap_NullPriorityComparer(Func<IComparer<B>, IRawHeap<A, B>> ctor)
    {
        ctor(null);
    }

    private static void NewRawIntHeap_CheckDefaultComparer(Func<IRawHeap<A, int>> ctor)
    {
        var heap = ctor();
        AssertHeapCorrectness(heap, BetterComparer<int>.Default);
    }

    private static void NewUniqueHeap_NullPriorityComparer(Func<IComparer<B>, IEqualityComparer<A>, IHeap<A, B>> ctor)
    {
        ctor(null, EqualityComparer<A>.Default);
    }

    private static void NewUniqueIntHeap_CheckDefaultComparer(Func<IEqualityComparer<A>, IHeap<A, int>> ctor)
    {
        var heap = ctor(EqualityComparer<A>.Default);
        AssertHeapCorrectness(heap, BetterComparer<int>.Default, EqualityComparer<A>.Default);
    }

    private abstract class A
    {
    }

    private abstract class B
    {
    }
}
