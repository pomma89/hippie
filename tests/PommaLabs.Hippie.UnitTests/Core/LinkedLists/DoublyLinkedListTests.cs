﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal abstract class DoublyLinkedListTests<TList> : SinglyLinkedListTests<TList> where TList : class, IDoublyLinkedList<int>
{
    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Append_Doubly_DifferentItems(int itemCount)
    {
        var otherList = GetList();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
            otherList.Add(itemCount + i);
        }
        _list.Append(otherList);
        for (var i = 0; i < itemCount * 2; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(2 * itemCount - i));
            _list.RemoveFirst();
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Append_Doubly_SameItems(int itemCount)
    {
        var otherList = GetList();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
            otherList.Add(i);
        }
        _list.Append(otherList);
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(2 * itemCount - i));
            _list.RemoveFirst();
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void GetReversedEnumerator_EmptyList()
    {
        Assert.That(_list.GetReversedEnumerator().MoveNext(), Is.False);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void GetReversedEnumerator_FullList(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        var revEn = _list.GetReversedEnumerator();
        for (var i = itemCount - 1; i >= 0; --i)
        {
            Assert.That(revEn.MoveNext(), Is.True);
            Assert.That(revEn.Current, Is.EqualTo(i));
        }
    }

    [Test]
    public void GetReversedEnumerator_NotNull_EmptyList()
    {
        Assert.That(_list.GetReversedEnumerator(), Is.Not.Null);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void GetReversedEnumerator_NotNull_FullList(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.That(_list.GetReversedEnumerator(), Is.Not.Null);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveLast_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.Last, Is.EqualTo(itemCount - i - 1));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveLast();
        }
    }

    [Test]
    public void RemoveLast_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.RemoveLast());
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveLast_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(0);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.Last, Is.EqualTo(0));
            Assert.That(_list.Count, Is.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Reverse_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.DoesNotThrow(() => _list.Reverse());
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Reverse_SameItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(0);
        }
        Assert.DoesNotThrow(() => _list.Reverse());
    }

    protected override TList GetList()
    {
        var lst = (TList)(new DoublyLinkedList<int>() as IDoublyLinkedList<int>);
        IDoublyLinkedList<int> ret = new MockedDoublyLinkedList<TList, int>(lst);
        return (TList)ret;
    }

    protected override TList GetList(IEqualityComparer<int> equalityComparer)
    {
        var lst = (TList)(new DoublyLinkedList<int>(equalityComparer) as IDoublyLinkedList<int>);
        IDoublyLinkedList<int> ret = new MockedDoublyLinkedList<TList, int>(lst);
        return (TList)ret;
    }
}

[TestFixture]
internal sealed class DoublyLinkedListTests : DoublyLinkedListTests<IDoublyLinkedList<int>>
{
}
