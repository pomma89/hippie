﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections;
using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal class MockedThinLinkedList<TList, TItem>(TList testList) : IThinLinkedList<TItem> where TList : class, IThinLinkedList<TItem>
{
    protected readonly LinkedList<TItem> RefList = new();
    protected readonly TList TestList = testList;

    public int Count
    {
        get { return TestList.Count; }
    }

    public IEqualityComparer<TItem> EqualityComparer
    {
        get { return TestList.EqualityComparer; }
    }

    public bool IsReadOnly
    {
        get { return TestList.IsReadOnly; }
    }

    public TItem First
    {
        get { return TestList.First; }
    }

    public virtual void Add(TItem item)
    {
        AddFirst(item);
    }

    public void AddFirst(TItem item)
    {
        TestList.AddFirst(item);
        RefList.AddFirst(item);
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
    }

    public void Clear()
    {
        TestList.Clear();
        RefList.Clear();
    }

    public bool Contains(TItem item)
    {
        var r1 = TestList.Contains(item);
        var r2 = TestList.Contains(item);
        Assert.That(r1, Is.EqualTo(r2));
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        return r1;
    }

    public void CopyTo(TItem[] array, int arrayIndex)
    {
        throw new NotImplementedException();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public IEnumerator<TItem> GetEnumerator()
    {
        return TestList.GetEnumerator();
    }

    public virtual bool Remove(TItem item)
    {
        var r1 = TestList.Remove(item);
        var r2 = RefList.Remove(item);
        Assert.That(r1, Is.EqualTo(r2));
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        if (TestList.Count > 0)
        {
            Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
        }
        return r1;
    }

    public TItem RemoveFirst()
    {
        var first = TestList.RemoveFirst();
        RefList.RemoveFirst();
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        if (TestList.Count > 0)
        {
            Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
        }
        return first;
    }
}
