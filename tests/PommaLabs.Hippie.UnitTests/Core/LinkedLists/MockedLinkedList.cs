﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

internal class MockedLinkedList<TList, TItem>(TList testList) : MockedThinLinkedList<TList, TItem>(testList), ILinkedList<TItem>
    where TList : class, ILinkedList<TItem>
{
    public TItem Last
    {
        get { return TestList.Last; }
    }

    public sealed override void Add(TItem item)
    {
        AddLast(item);
    }

    public void AddLast(TItem item)
    {
        TestList.AddLast(item);
        RefList.AddLast(item);
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        Assert.That(TestList.Last, Is.EqualTo(RefList.Last.Value));
    }

    public void Append(ILinkedList<TItem> list)
    {
        if (list is not null)
        {
            foreach (var item in list)
            {
                RefList.AddLast(item);
            }
        }
        TestList.Append(list);
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
        Assert.That(TestList.Last, Is.EqualTo(RefList.Last.Value));
    }

    public override bool Remove(TItem item)
    {
        var r1 = TestList.Remove(item);
        var r2 = RefList.Remove(item);
        Assert.That(r1, Is.EqualTo(r2));
        Assert.That(TestList.Count, Is.EqualTo(RefList.Count));
        if (TestList.Count > 0)
        {
            Assert.That(TestList.First, Is.EqualTo(RefList.First.Value));
            Assert.That(TestList.Last, Is.EqualTo(RefList.Last.Value));
        }
        return r1;
    }
}
