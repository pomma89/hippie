﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

[TestFixture]
internal sealed class LinkedStackTests
{
    [SetUp]
    public void SetUp()
    {
        _stack = new LinkedStack<int>();
    }

    [TearDown]
    public void TearDown()
    {
        _stack = null;
    }

    private const int SmallCount = 10;
    private const int MediumCount = 100;
    private const int BigCount = 1000;

    private LinkedStack<int> _stack;

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void SimpleTest(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _stack.Push(i);
        }
        Assert.That(_stack, Has.Count.EqualTo(itemCount));
        for (var i = itemCount - 1; i >= 0; --i)
        {
            Assert.That(_stack.Top(), Is.EqualTo(i));
            Assert.That(_stack.Pop(), Is.EqualTo(i));
            Assert.That(_stack, Has.Count.EqualTo(i));
        }
    }
}
