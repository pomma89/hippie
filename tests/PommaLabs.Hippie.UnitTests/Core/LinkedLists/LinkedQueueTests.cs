﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

[TestFixture]
internal sealed class LinkedQueueTests
{
    [SetUp]
    public void SetUp()
    {
        _queue = new LinkedQueue<int>();
    }

    [TearDown]
    public void TearDown()
    {
        _queue = null;
    }

    private const int SmallCount = 10;
    private const int MediumCount = 100;
    private const int BigCount = 1000;

    private LinkedQueue<int> _queue;

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void SimpleTest(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _queue.Enqueue(i);
        }
        Assert.That(_queue, Has.Count.EqualTo(itemCount));
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_queue.Peek(), Is.EqualTo(i));
            Assert.That(_queue.Dequeue(), Is.EqualTo(i));
            Assert.That(_queue, Has.Count.EqualTo(itemCount - i - 1));
        }
    }
}
