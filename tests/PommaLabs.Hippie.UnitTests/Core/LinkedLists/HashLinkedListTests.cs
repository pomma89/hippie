﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using NUnit.Framework;
using PommaLabs.Hippie.Core.LinkedLists;
using Shouldly;

namespace PommaLabs.Hippie.UnitTests.Core.LinkedLists;

[TestFixture]
internal sealed class HashLinkedListTests
{
    private const int BigCount = 1000;

    private const int MediumCount = 100;

    private const int SmallCount = 10;

    private HashLinkedList<int> _list;

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Add_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.That(_list, Has.Count.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddAfter_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddAfter(i, itemCount + i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            _list.RemoveFirst();
            Assert.That(_list.First, Is.EqualTo(itemCount + i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void AddAfter_EmptyList()
    {
        Should.Throw<ArgumentException>(() => _list.AddAfter(5, 7));
    }

    [Test]
    public void AddAfter_ItemNotContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.AddAfter(2, 8));
    }

    [Test]
    public void AddAfter_NewItemContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.AddAfter(1, 5));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddBefore_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddBefore(i, itemCount + i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(itemCount + i));
            _list.RemoveFirst();
            Assert.That(_list.First, Is.EqualTo(i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void AddBefore_EmptyList()
    {
        Should.Throw<ArgumentException>(() => _list.AddBefore(5, 7));
    }

    [Test]
    public void AddBefore_ItemNotContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.AddBefore(2, 8));
    }

    [Test]
    public void AddBefore_NewItemContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.AddBefore(5, 1));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddFirst_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        Assert.That(_list, Has.Count.EqualTo(itemCount));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void AddLast_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
        }
        Assert.That(_list, Has.Count.EqualTo(itemCount));
    }

    [Test]
    public void Factory_CustomEqualityComparer()
    {
        var eqCmp = new DummyEqualityComparer();
        Assert.That(eqCmp, Is.SameAs(new HashLinkedList<int>(eqCmp).EqualityComparer));
    }

    [Test]
    public void Factory_DefaultEqualityComparer()
    {
        Assert.That(EqualityComparer<int>.Default, Is.SameAs(new HashLinkedList<int>().EqualityComparer));
    }

    [Test]
    public void Factory_NullEqualityComparer()
    {
        Should.Throw<ArgumentNullException>(() => new HashLinkedList<int>(null));
    }

    [Test]
    public void First_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.First);
    }

    [Test]
    public void First_NoItems_AfterAddRemove()
    {
        _list.Add(5);
        _list.RemoveFirst();
        Should.Throw<InvalidOperationException>(() => _list.First);
    }

    [Test]
    public void GetReversedEnumerator_EmptyList()
    {
        Assert.That(_list.GetReversedEnumerator().MoveNext(), Is.False);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void GetReversedEnumerator_FullList(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        var revEn = _list.GetReversedEnumerator();
        for (var i = itemCount - 1; i >= 0; --i)
        {
            Assert.That(revEn.MoveNext(), Is.True);
            Assert.That(revEn.Current, Is.EqualTo(i));
        }
    }

    [Test]
    public void GetReversedEnumerator_NotNull_EmptyList()
    {
        Assert.That(_list.GetReversedEnumerator(), Is.Not.Null);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void GetReversedEnumerator_NotNull_FullList(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.That(_list.GetReversedEnumerator(), Is.Not.Null);
    }

    [Test]
    public void Last_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.Last);
    }

    [Test]
    public void Last_NoItems_AfterAddRemove()
    {
        _list.Add(5);
        _list.RemoveFirst();
        Should.Throw<InvalidOperationException>(() => _list.Last);
    }

    [Test]
    public void Remove_ContainedItem()
    {
        _list.Add(0);
        Assert.That(_list.Remove(0), Is.True);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_DifferentItems_RandomOrder(int itemCount)
    {
        var heap = new SortedList<int, int>();
        var rand = new Random();
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
            int key;
            do { key = rand.Next(); } while (heap.ContainsKey(key));
            heap.Add(key, i);
        }
        foreach (var i in heap)
        {
            Assert.That(_list.Remove(i.Value), Is.True);
        }
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Remove_DifferentItems_SameOrder(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddFirst(i);
        }
        for (var i = itemCount - 1; i > 0; --i)
        {
            Assert.That(_list.Remove(i), Is.True);
            Assert.That(_list.First, Is.EqualTo(i - 1));
        }
        Assert.That(_list.Remove(0), Is.True);
    }

    [Test]
    public void Remove_NotContainedItem()
    {
        Assert.That(_list.Remove(0), Is.False);
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveAfter_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(i);
            _list.AddLast(itemCount + i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            _list.RemoveAfter(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list, Has.Count.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void RemoveAfter_EmptyList()
    {
        Should.Throw<ArgumentException>(() => _list.RemoveAfter(5));
    }

    [Test]
    public void RemoveAfter_ItemNotContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.RemoveAfter(2));
    }

    [Test]
    public void RemoveAfter_LastItem()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<InvalidOperationException>(() => _list.RemoveAfter(5));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveBefore_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.AddLast(itemCount + i);
            _list.AddLast(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            _list.RemoveBefore(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list, Has.Count.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void RemoveBefore_EmptyList()
    {
        Should.Throw<ArgumentException>(() => _list.RemoveBefore(5));
    }

    [Test]
    public void RemoveBefore_FirstItem()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<InvalidOperationException>(() => _list.RemoveBefore(1));
    }

    [Test]
    public void RemoveBefore_ItemNotContained()
    {
        _list.Add(1);
        _list.Add(3);
        _list.Add(5);
        Should.Throw<ArgumentException>(() => _list.RemoveBefore(2));
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveFirst_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.First, Is.EqualTo(i));
            Assert.That(_list, Has.Count.EqualTo(itemCount - i));
            _list.RemoveFirst();
        }
    }

    [Test]
    public void RemoveFirst_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.RemoveFirst());
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void RemoveLast_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        for (var i = 0; i < itemCount; ++i)
        {
            Assert.That(_list.Last, Is.EqualTo(itemCount - i - 1));
            Assert.That(_list, Has.Count.EqualTo(itemCount - i));
            _list.RemoveLast();
        }
    }

    [Test]
    public void RemoveLast_NoItems()
    {
        Should.Throw<InvalidOperationException>(() => _list.RemoveLast());
    }

    [TestCase(SmallCount)]
    [TestCase(MediumCount)]
    [TestCase(BigCount)]
    public void Reverse_DifferentItems(int itemCount)
    {
        for (var i = 0; i < itemCount; ++i)
        {
            _list.Add(i);
        }
        Assert.DoesNotThrow(() => _list.Reverse());
    }

    [SetUp]
    public void SetUp()
    {
        _list = [];
    }

    [TearDown]
    public void TearDown()
    {
        _list = null;
    }

    private sealed class DummyEqualityComparer : IEqualityComparer<int>
    {
        public bool Equals(int x, int y)
        {
            return true;
        }

        public int GetHashCode(int obj)
        {
            return 0;
        }
    }
}
