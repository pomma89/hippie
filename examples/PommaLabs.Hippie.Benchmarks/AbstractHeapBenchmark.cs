﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using BenchmarkDotNet.Attributes;

namespace PommaLabs.Hippie.Benchmarks;

[Config(typeof(Program.Config))]
public abstract class AbstractHeapBenchmark
{
    private readonly Random _random = new();

    [Params("Binary", "Binomial", "Fibonacci", "Pairing", "Ternary")]
    public string HeapType { get; set; }

    [Params(100, 1000, 10000)]
    public int ItemCount { get; set; }

    protected void AddOrderedItemsHelper(IHeap<int, int> heap)
    {
        for (var i = 0; i < ItemCount; ++i)
        {
            heap.Add(i, i);
        }
    }

    protected void AddRandomItemsHelper(IHeap<int, int> heap, int startIndex = 0)
    {
        for (var i = startIndex; i < startIndex + ItemCount; ++i)
        {
            heap.Add(i, _random.Next());
        }
    }

    protected void AddReverseOrderedItemsHelper(IHeap<int, int> heap)
    {
        for (var i = ItemCount - 1; i >= 0; --i)
        {
            heap.Add(i, i);
        }
    }

    protected IHeap<int, int> GetHeap()
    {
        return HeapType switch
        {
            "Binary" => HeapFactory.NewBinaryHeap<int, int>(),
            "Binomial" => HeapFactory.NewBinomialHeap<int, int>(),
            "Fibonacci" => HeapFactory.NewFibonacciHeap<int, int>(),
            "Pairing" => HeapFactory.NewPairingHeap<int, int>(),
            "Ternary" => HeapFactory.NewArrayHeap<int, int>(childCount: 3),
            _ => throw new InvalidOperationException()
        };
    }

    protected static void RemoveMinUntilEmptyHelper(IHeap<int, int> heap)
    {
        while (heap.Count != 0)
        {
            heap.RemoveMin();
        }
    }
}
