﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using BenchmarkDotNet.Attributes;

namespace PommaLabs.Hippie.Benchmarks;

public class MergeBenchmark : AbstractHeapBenchmark
{
    private IHeap<int, int> _heap;
    private IHeap<int, int> _otherHeap;

    [IterationSetup]
    public void IterationCleanup()
    {
        _heap.Clear();
        _heap = null;

        _otherHeap.Clear();
        _otherHeap = null;
    }

    [IterationSetup]
    public void IterationSetup()
    {
        _heap = GetHeap();
        AddRandomItemsHelper(_heap);

        _otherHeap = GetHeap();
        AddRandomItemsHelper(_otherHeap, startIndex: ItemCount);
    }

    [Benchmark]
    public void MergeTwoHeaps()
    {
        _heap.Merge(_otherHeap);
    }
}
