﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;

namespace PommaLabs.Hippie.Examples;

internal static class Dijkstra
{
    private static int[] Traverse(IRawHeap<int, int> heap, IList<IEnumerable<Edge>> edges, int start, int nodeCount)
    {
        var distances = new int[nodeCount];
        var visited = new bool[nodeCount];
        var nodes = new IHeapHandle<int, int>[nodeCount];
        for (var i = 0; i < nodeCount; ++i)
        {
            nodes[i] = heap.Add(i, int.MaxValue);
            distances[i] = int.MaxValue;
        }
        heap[nodes[start]] = 0;

        while (heap.Count != 0)
        {
            var u = heap.RemoveMin();
            if (u.Priority == int.MaxValue)
            {
                break;
            }
            var uId = u.Value;
            distances[uId] = u.Priority;
            visited[uId] = true;
            foreach (var e in edges[uId])
            {
                if (visited[e.Target])
                {
                    continue;
                }
                var tmpDist = u.Priority + e.Length;
                var v = nodes[e.Target];
                if (tmpDist < v.Priority)
                {
                    heap[v] = tmpDist;
                }
            }
        }

        return distances;
    }

    private struct Edge
    {
        public int Length { get; set; }
        public int Target { get; set; }
    }
}
