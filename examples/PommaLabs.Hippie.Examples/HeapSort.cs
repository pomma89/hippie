﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Hippie.Examples;

internal static class HeapSort
{
    public static void Run()
    {
        var ints = Sort(new[] { 9, 8, 7, 6, 5, 4, 3, 2, 1 });
        foreach (var i in ints)
        {
            Console.Write(i + " "); // Expected output: 1 2 3 4 5 6 7 8 9
        }
        Console.WriteLine();

        var cmp = new ReversedIntComparer();
        ints = Sort(new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, cmp);
        foreach (var i in ints)
        {
            Console.Write(i + " "); // Expected output: 9 8 7 6 5 4 3 2 1
        }
        Console.WriteLine();
    }

    private static IList<T> Sort<T>(IEnumerable<T> elems) where T : IComparable<T>
    {
        var heap = HeapFactory.NewBinaryHeap<T>();
        foreach (var elem in elems)
        {
            heap.Add(elem);
        }
        var list = new List<T>(heap.Count);
        while (heap.Count != 0)
        {
            list.Add(heap.RemoveMin());
        }
        return list;
    }

    private static IList<T> Sort<T>(IEnumerable<T> elems, IComparer<T> cmp)
    {
        var heap = HeapFactory.NewBinaryHeap(cmp);
        foreach (var elem in elems)
        {
            heap.Add(elem);
        }
        var list = new List<T>(heap.Count);
        while (heap.Count != 0)
        {
            list.Add(heap.RemoveMin());
        }
        return list;
    }

    private sealed class ReversedIntComparer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            return -1 * x.CompareTo(y);
        }
    }
}
