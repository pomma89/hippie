﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.Hippie.Examples;

internal sealed class RawMessage
{
    private RawMessage(string msg)
    {
        Message = msg;
    }

    private string Message { get; set; }

    public static void Run()
    {
        var raw = HeapFactory.NewRawBinaryHeap<RawMessage, int>();
        var h1 = raw.Add(new RawMessage("Urgent message"), -100);
        var h2 = raw.Add(new RawMessage("Urgent message"), -50);
        raw.Add(new RawMessage("Silly message"), 10);

        // We update h1, it is not urgent anymore
        h1.Value.Message = "Not urgent anymore";
        raw.UpdatePriorityOf(h1, 200);

        // We update h2, it is more urgent!
        raw.UpdatePriorityOf(h2, -2000);

        while (raw.Count > 0)
        {
            var min = raw.RemoveMin();
            Console.WriteLine("Msg: ({0}), Weight: {1}", min.Value.Message, min.Priority);
            // Expected output:
            // Msg: (Urgent message), Weight: -2000;
            // Msg: (Silly message), Weight: 10
            // Msg: (Not urgent anymore), Weight: 200
        }
    }
}
