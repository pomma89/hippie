﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.Hippie.Examples;

internal static class StableHeap
{
    public static void Run()
    {
        const int ItemCount = 10;
        var stableHeap = StableHeapFactory.NewBinaryHeap<int, int>();

        for (var i = 0; i < ItemCount; ++i)
        {
            stableHeap.Add(i, 0);
        }
        for (var i = 0; i < ItemCount; ++i)
        {
            // Expected output: 0 1 2 3 4 5 6 7 8 9
            Console.Write(stableHeap.RemoveMin().Value + " ");
        }
        Console.WriteLine();
    }
}
