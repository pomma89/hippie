﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;

namespace PommaLabs.Hippie.Examples;

internal sealed class WeightedMessage : IEquatable<WeightedMessage>, IComparable<WeightedMessage>
{
    private WeightedMessage(string msg, int weight)
    {
        Message = msg;
        Weight = weight;
    }

    private string Message { get; set; }

    private int Weight { get; set; }

    public static void Run()
    {
        var multi = HeapFactory.NewBinaryHeap<WeightedMessage>();
        multi.Add(new WeightedMessage("Important message", 0));
        multi.Add(new WeightedMessage("Silly message", 100));
        multi.Add(new WeightedMessage("Urgent message", -10));
        multi.Add(new WeightedMessage("Urgent message", -50));
        multi.Add(new WeightedMessage("Important message", 0));
        while (multi.Count > 0)
        {
            var min = multi.RemoveMin();
            Console.WriteLine("Msg: ({0}), Weight: {1}", min.Message, min.Weight);
            // Expected output:
            // Msg: (Urgent message), Weight: -50
            // Msg: (Urgent message), Weight: -10
            // Msg: (Important message), Weight: 0
            // Msg: (Important message), Weight: 0
            // Msg: (Silly message), Weight: 100
        }
    }

    public override bool Equals(object obj)
    {
        return obj is not null && (ReferenceEquals(this, obj) || Equals(obj as WeightedMessage));
    }

    public bool Equals(WeightedMessage other)
    {
        return string.Equals(Message, other.Message);
    }

    public override int GetHashCode()
    {
        return Message.GetHashCode();
    }

    public int CompareTo(WeightedMessage other)
    {
        return Weight.CompareTo(other.Weight);
    }
}
