﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace PommaLabs.Hippie;

/// <summary>
///   A class offering a better default comparer for given type parameter
///   <typeparamref name="T"/>. <br/> We need a particular version of <see cref="Comparer{T}"/>,
///   because in a situation like the next one: ~~~{.cs} class A : IComparable{A} { ... } class
///   B : A { ... } ~~~ We have that: ~~~{.cs} ReferenceEquals(Comparer{A}.Default,
///   Comparer{B}.Default) == false ~~~ When we need that statement to be true, in order to
///   allow meaningful merge operations. So, this class grants the following relation: ~~~{.cs}
///   ReferenceEquals(BetterComparer{A}.Default, BetterComparer{B}.Default) == true ~~~
/// </summary>
public static class BetterComparer<T> where T : IComparable<T>
{
    /// <summary>
    ///   The default comparer for <typeparamref name="T"/>.
    /// </summary>
    public static IComparer<T> Default { get; } = GetDefaultComparer();

    private static IComparer<T> GetDefaultComparer()
    {
        var type = typeof(T);
        var comparable = typeof(IComparable<T>);
        if (Array.Find(type.GetInterfaces(), i => ReferenceEquals(i, comparable)) != null)
        {
            return new DefaultComparer();
        }
        var baseComparer = typeof(BetterComparer<>).MakeGenericType(type.BaseType);
        var defaultComparer = baseComparer.GetProperties(BindingFlags.Public | BindingFlags.Static)[0];
        return (IComparer<T>)defaultComparer.GetValue(null, null);
    }

    private sealed class DefaultComparer : IComparer<T>
    {
        public int Compare(T x, T y)
        {
            return x.CompareTo(y);
        }
    }
}
