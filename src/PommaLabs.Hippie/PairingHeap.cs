﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using PommaLabs.Hippie.Core;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie;

public sealed class PairingHeap<TVal, TPr> : TreePoolHeap<TVal, TPr>
{
    private readonly LinkedQueue<Tree> _auxQueue = new();
    private readonly LinkedStack<Tree> _auxStack = new();
    private Tree _mainTree;

    internal PairingHeap(IComparer<TPr> comparer)
        : base(comparer)
    {
    }

    public override void Clear()
    {
        base.Clear();
        _mainTree = null;
    }

    public override void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    public override IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        if (Count == 0)
        {
            yield break;
        }
        foreach (var tree in TreePool.SelectMany(t => t.BreadthFirstVisit()))
        {
            yield return tree.Handle;
        }
        if (_mainTree == null)
        {
            yield break;
        }
        foreach (var tree in _mainTree.BreadthFirstVisit())
        {
            yield return tree.Handle;
        }
    }

    public override void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        if (other is not PairingHeap<TVal, TPr> otherPairingHeap)
        {
            this.CommonMerge(other);
            return;
        }
        otherPairingHeap.Version.Id = Version.Id; // Updates all other nodes version
        TreePool.Append(otherPairingHeap.TreePool);
        if (otherPairingHeap._mainTree != null)
        {
            TreePool.AddLast(otherPairingHeap._mainTree);
        }
        FixMin(otherPairingHeap.MinTree);
        Count += otherPairingHeap.Count;
        otherPairingHeap.Clear();
    }

    public override bool Remove(IHeapHandle<TVal, TPr> item)
    {
        var treeHandle = GetHandle(item as TreeHandle);
        if (treeHandle == null)
        {
            return false;
        }
        var tree = treeHandle.Tree;
        // We put this instuction here because the object referenced by tree may be changed by
        // the instructions below.
        tree.Handle.Version = null;
        if (ReferenceEquals(tree, MinTree))
        {
            RemoveMinTree(tree);
        }
        else
        {
            RemoveTree(tree);
        }
        Count--;
        return true;
    }

    public override IHeapHandle<TVal, TPr> RemoveMin()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

        var oldMin = MinTree;
        var min = NullMeld(MultiPassPairing(), _mainTree);
        if (!ReferenceEquals(min, oldMin))
        {
            min.SwapRootWith(oldMin);
        }
        MinTree = SetMainTree(TwoPassPairing(min.Children));
        Count--;
        min.Handle.Version = null;
        return min.Handle;
    }

    public override IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        if (Count == 0)
        {
            yield break;
        }
        foreach (var tree in TreePool)
        {
            yield return tree.ToReadOnlyTree();
        }
        if (_mainTree != null)
        {
            yield return _mainTree.ToReadOnlyTree();
        }
    }

    public override string ToString()
    {
        return this.CommonToString();
    }

    protected override void MoveDown(TreeHandle handle)
    {
        var tree = handle.Tree;
        if (tree.Children.Count > 0)
        {
            foreach (var child in tree.Children)
            {
                child.Parent = null;
            }
            TreePool.Append(tree.Children);
        }
        if (!ReferenceEquals(tree, MinTree))
        {
            return;
        }
        MinTree = SetMainTree(NullMeld(MultiPassPairing(), _mainTree));
    }

    protected override void MoveUp(TreeHandle handle)
    {
        var tree = handle.Tree;
        var parent = tree.Parent;
        if (parent != null)
        {
            parent.Children.Remove(tree);
            tree.Parent = null;
            TreePool.AddLast(tree);
        }
        FixMin(tree);
    }

    private protected override void FixMin(Tree tree)
    {
        if (MinTree != null && Compare(MinTree.Priority, tree.Priority) < 0)
        {
            return;
        }
        Debug.Assert(tree.Parent == null);
        MinTree = tree;
    }

    private Tree MultiPassPairing()
    {
        if (TreePool.Count == 0)
        {
            return null;
        }
        foreach (var tree in TreePool)
        {
            _auxQueue.Enqueue(tree);
        }
        TreePool.Clear();
        while (_auxQueue.Count >= 2)
        {
            _auxQueue.Enqueue(Meld(_auxQueue.Dequeue(), _auxQueue.Dequeue()));
        }
        return _auxQueue.Dequeue();
    }

    private void RemoveMinTree(Tree tree)
    {
        // Given tree is the minimum, so we can use the same procedure used within
        // RemoveMin, with a small difference. See below...
        var min = NullMeld(MultiPassPairing(), _mainTree);
        MinTree = SetMainTree(TwoPassPairing(min.Children));
        // If the new min is not the old min (this can happen when two or more nodes have
        // the same priority), we need to ensure given value is really the value we are
        // going to delete. To do so, we swap values between tree and min.
        if (!ReferenceEquals(tree, min))
        {
            tree.SwapRootWith(min);
        }
    }

    private void RemoveTree(Tree tree)
    {
        var pairedChildren = TwoPassPairing(tree.Children);
        if (ReferenceEquals(tree, _mainTree))
        {
            SetMainTree(pairedChildren);
        }
        else if (tree.Parent == null)
        {
            TreePool.Remove(tree);
            if (pairedChildren != null)
            {
                pairedChildren.Parent = null;
                TreePool.AddLast(pairedChildren);
            }
        }
        else
        {
            var parent = tree.Parent;
            var grandParent = parent.Parent;
            parent.Children.Remove(tree);
            var meld = NullMeld(parent, pairedChildren);
            if (ReferenceEquals(parent, _mainTree))
            {
                SetMainTree(meld);
            }
            else if (grandParent == null)
            {
                TreePool.Remove(parent);
                meld.Parent = null;
                TreePool.AddLast(meld);
            }
            else
            {
                grandParent.Children.Remove(parent);
                meld.Parent = grandParent;
                grandParent.Children.AddLast(meld);
            }
        }
    }

    private Tree SetMainTree(Tree value)
    {
        _mainTree = value;
        if (_mainTree != null)
        {
            _mainTree.Parent = null;
        }
        return value;
    }

    private Tree TwoPassPairing(ICollection<Tree> trees)
    {
        if (trees.Count == 0)
        {
            return null;
        }
        var en = trees.GetEnumerator();
        while (en.MoveNext())
        {
            var tmp = en.Current;
            _auxStack.Push(en.MoveNext() ? Meld(tmp, en.Current) : tmp);
        }
        var merged = _auxStack.Pop();
        while (_auxStack.Count != 0)
        {
            merged = Meld(merged, _auxStack.Pop());
        }
        return merged;
    }
}
