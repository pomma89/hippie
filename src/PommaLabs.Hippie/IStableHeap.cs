﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Hippie;

/// <summary>
///   A generic stable heap that supports all the operations that can be done in a reasonable
///   amount of time on that data structure. <br/> This stable heap is an enumerable collection
///   of heap handles; however, pairs are _not_ granted to be visited in the order determined by <see cref="IThinHeap{TVal, TPr}.Comparer"/>.
/// </summary>
/// <typeparam name="TVal">The type of the values contained in the stable heap.</typeparam>
/// <typeparam name="TPr">
///   The type of the priorities associated with the values contained in the stable heap.
/// </typeparam>
/// <remarks>
///   A stable raw heap allows the presence of duplicate values. Moreover, null values are
///   allowed, while null priorities are not (to avoid issues with comparers).
/// </remarks>
public interface IStableRawHeap<TVal, TPr> : IRawHeap<TVal, TPr>, IStableThinHeap<TVal, TPr>
{
    /// <summary>
    ///   Updates the priority associated with given handle and returns the old priority.
    /// </summary>
    /// <value>The new priority to associate with given handle.</value>
    /// <param name="handle">The handle to update.</param>
    /// <returns>The priority previously associated with given handle.</returns>
    /// <exception cref="ArgumentException">Given handle does not belong to this heap.</exception>
    /// <exception cref="ArgumentNullException">
    ///   <paramref name="handle"/> or <paramref name="value"/> are null.
    /// </exception>
    TPr this[IHeapHandle<TVal, IVersionedPriority<TPr>> handle] { set; }

    /// <summary>
    ///   Adds an handle with given value and given priority to the heap.
    /// </summary>
    /// <param name="value">The value to be added.</param>
    /// <param name="priority">The priority associated with given value.</param>
    /// <returns>An handle with allows to "edit" the pair added.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="priority"/> is null.</exception>
    new IHeapHandle<TVal, IVersionedPriority<TPr>> Add(TVal value, TPr priority);

    /// <summary>
    ///   Adds an handle with given value and given priority to the heap. Specified version will
    ///   be used, instead of the default one.
    /// </summary>
    /// <param name="value">The value to be added.</param>
    /// <param name="priority">The priority associated with given value.</param>
    /// <param name="version">The version associated with given value.</param>
    /// <returns>An handle with allows to "edit" the pair added.</returns>
    /// <exception cref="ArgumentNullException"><paramref name="priority"/> is null.</exception>
    new IHeapHandle<TVal, IVersionedPriority<TPr>> Add(TVal value, TPr priority, long version);

    /// <summary>
    ///   Determines whether an element is in the heap.
    /// </summary>
    /// <param name="handle">The handle to be checked.</param>
    /// <returns>True if the element is contained, false otherwise.</returns>
    new bool Contains(IHeapHandle<TVal, IVersionedPriority<TPr>> handle);

    /// <summary>
    ///   Returns an enumerator that iterates through the heap.
    /// </summary>
    /// <returns>An enumerator that iterates through the heap.</returns>
    new IEnumerator<IHeapHandle<TVal, IVersionedPriority<TPr>>> GetEnumerator();

    /// <summary>
    ///   Removes the item corresponding to given handle from the heap.
    /// </summary>
    /// <param name="handle">The handle corresponding to the item to be checked.</param>
    /// <returns>True if the item was contained and removed, false otherwise.</returns>
    bool Remove(IHeapHandle<TVal, IVersionedPriority<TPr>> handle);

    /// <summary>
    ///   Updates the priority associated with given handle and returns the old priority.
    /// </summary>
    /// <param name="handle">The handle to update.</param>
    /// <param name="newPriority">The new priority to associate with given handle.</param>
    /// <returns>The priority previously associated with given handle.</returns>
    /// <exception cref="ArgumentException">Given handle does not belong to this heap.</exception>
    /// <exception cref="ArgumentNullException">
    ///   <paramref name="handle"/> or <paramref name="newPriority"/> are null.
    /// </exception>
    IVersionedPriority<TPr> UpdatePriorityOf(
        IHeapHandle<TVal, IVersionedPriority<TPr>> handle,
        TPr newPriority);

    /// <summary>
    ///   Updates the priority associated with given handle and returns the old priority.
    ///   Specified version will be used, instead of the default one.
    /// </summary>
    /// <param name="handle">The handle to update.</param>
    /// <param name="newPriority">The new priority to associate with given handle.</param>
    /// <param name="newVersion">The new version to associate with given handle.</param>
    /// <returns>The priority previously associated with given handle.</returns>
    /// <exception cref="ArgumentException">Given handle does not belong to this heap.</exception>
    /// <exception cref="ArgumentNullException">
    ///   <paramref name="handle"/> or <paramref name="newPriority"/> are null.
    /// </exception>
    IVersionedPriority<TPr> UpdatePriorityOf(
        IHeapHandle<TVal, IVersionedPriority<TPr>> handle,
        TPr newPriority, long newVersion);

    /// <summary>
    ///   Updates given handle with the new specified value.
    /// </summary>
    /// <param name="handle">The handle whose value has to be updated.</param>
    /// <param name="newValue">The new value that will replace given old value.</param>
    /// <returns>The value previously associated with given handle.</returns>
    /// <exception cref="ArgumentException">Given handle does not belong to this heap.</exception>
    /// <exception cref="ArgumentNullException"><paramref name="handle"/> is null.</exception>
    TVal UpdateValue(IHeapHandle<TVal, IVersionedPriority<TPr>> handle, TVal newValue);
}
