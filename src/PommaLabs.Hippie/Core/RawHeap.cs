﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace PommaLabs.Hippie.Core;

public abstract class RawHeap<TVal, TPr, TItem> : IRawHeap<TVal, TPr>
    where TItem : class, RawHeap<TVal, TPr, TItem>.IItem
{
    protected RawHeap(IComparer<TPr> comparer)
    {
        Comparer = comparer ?? throw new ArgumentNullException(nameof(comparer), ErrorMessages.NullComparer);
        Compare = comparer.Compare;
    }

    /// <inheritdoc/>
    public void Add(IHeapHandle<TVal, TPr> handle)
    {
        Add(handle.Value, handle.Priority);
    }

    /// <inheritdoc/>
    void IThinHeap<TVal, TPr>.Add(TVal value, TPr priority)
    {
        Add(value, priority);
    }

    public IComparer<TPr> Comparer { get; }

    public int Count { get; protected set; }

    public IEqualityComparer<TVal> EqualityComparer
    {
        get { return EqualityComparer<TVal>.Default; }
    }

    public bool IsReadOnly
    {
        get { return false; }
    }

    protected Func<TPr, TPr, int> Compare { get; }

    public TPr this[IHeapHandle<TVal, TPr> handle]
    {
        set { UpdatePriorityOf(handle, value); }
    }

    public bool Contains(IHeapHandle<TVal, TPr> handle)
    {
        return GetHandle(handle as TItem) != null;
    }

    public TPr UpdatePriorityOf(IHeapHandle<TVal, TPr> handle, TPr newPriority)
    {
        // Preconditions
        if (newPriority is null) throw new ArgumentNullException(nameof(newPriority), ErrorMessages.NullPriority);
        if (!Contains(handle)) throw new ArgumentException(ErrorMessages.NoHandle, nameof(handle));

        var item = handle as TItem;
        var oldPriority = item.Priority;
        item.Priority = newPriority;
        var comparer = Compare(newPriority, oldPriority);
        if (comparer < 0)
        {
            MoveUp(item);
        }
        else if (comparer > 0)
        {
            MoveDown(item);
        }
        var result = oldPriority;

        // Postconditions
        Debug.Assert(Contains(handle));
        Debug.Assert(Comparer.Compare(handle.Priority, newPriority) == 0);

        return result;
    }

    public TVal UpdateValue(IHeapHandle<TVal, TPr> handle, TVal newValue)
    {
        // Preconditions
        if (!Contains(handle)) throw new ArgumentException(ErrorMessages.NoHandle, nameof(handle));

        var item = handle as TItem;
        Debug.Assert(item != null);
        var oldValue = item.Value;
        item.Value = newValue;
        var result = oldValue;

        // Postconditions
        Debug.Assert(Contains(handle));
        Debug.Assert(EqualityComparer.Equals(handle.Value, newValue));

        return result;
    }

    public abstract IHeapHandle<TVal, TPr> Min { get; }

    protected abstract TItem GetHandle(TItem item);

    protected abstract void MoveDown(TItem item);

    protected abstract void MoveUp(TItem item);

    public abstract IHeapHandle<TVal, TPr> Add(TVal value, TPr priority);

    public abstract void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
        where TVal2 : TVal
        where TPr2 : TPr;

    public abstract IHeapHandle<TVal, TPr> RemoveMin();

    public abstract IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest();

    public abstract void Clear();

    public abstract void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex);

    public abstract bool Remove(IHeapHandle<TVal, TPr> item);

    public abstract IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
    {
        throw new NotImplementedException();
    }

    public interface IItem
    {
        TPr Priority { get; set; }

        TVal Value { get; set; }
    }
}
