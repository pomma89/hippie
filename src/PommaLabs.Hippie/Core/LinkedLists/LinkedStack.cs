﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the stack will contain.</typeparam>
public sealed class LinkedStack<T> : ILinkedStack<T>
{
    #region Fields

    private SinglyNode<T> _firstNode;

    #endregion Fields

    #region Construction

    /// <summary>
    ///   Returns a stack implemented using an <see cref="IThinLinkedList{TItem}"/>.
    /// </summary>
    /// <returns>A stack implemented using an <see cref="IThinLinkedList{TItem}"/>.</returns>
    public LinkedStack()
    {
        // Postconditions
        Debug.Assert(Count == 0);
    }

    #endregion Construction

    #region IEnumerable Members

    /// <inheritdoc/>
    public IEnumerator<T> GetEnumerator()
    {
        for (var n = _firstNode; n != null; n = n.Next)
        {
            yield return n.Item;
        }
    }

    /// <inheritdoc/>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    #endregion IEnumerable Members

    #region ILinkedStack Members

    /// <inheritdoc/>
    public int Count { get; private set; }

    /// <inheritdoc/>
    public T Pop()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyStack);

        var first = _firstNode.Item;
        _firstNode = _firstNode.Next;
        Count--;
        return first;
    }

    /// <inheritdoc/>
    public void Push(T item)
    {
        _firstNode = new SinglyNode<T>(item, _firstNode);
        Count++;
    }

    /// <inheritdoc/>
    public T Top()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyStack);

        return _firstNode.Item;
    }

    #endregion ILinkedStack Members
}
