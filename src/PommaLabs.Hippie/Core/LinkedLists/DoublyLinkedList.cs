﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the list will contain.</typeparam>
public sealed class DoublyLinkedList<T> : ListBase<DoublyNode<T>, T>, IDoublyLinkedList<T>
{
    #region Construction

    /// <summary>
    ///   Returns the default implementation of the <see cref="IDoublyLinkedList{TItem}"/> interface.
    /// </summary>
    /// <returns>An implementation of the <see cref="IDoublyLinkedList{TItem}"/> interface.</returns>
    public DoublyLinkedList()
        : base(EqualityComparer<T>.Default)
    {
    }

    /// <summary>
    ///   Returns the default implementation of the <see cref="IDoublyLinkedList{TItem}"/>
    ///   interface, using specified equality comparer.
    /// </summary>
    /// <param name="equalityComparer">
    ///   The equality comparer that it will be used to determine whether two items are equal.
    /// </param>
    /// <returns>
    ///   An implementation of the <see cref="IDoublyLinkedList{TItem}"/> interface using
    ///   specified equality comparer.
    /// </returns>
    public DoublyLinkedList(IEqualityComparer<T> equalityComparer)
        : base(equalityComparer)
    {
    }

    #endregion Construction

    #region ICollection Members

    /// <inheritdoc/>
    public override void Add(T item)
    {
        AddLast(item);
    }

    #endregion ICollection Members

    #region IDoublyLinkedList Members

    /// <inheritdoc/>
    public override void AddFirst(T item)
    {
        var node = new DoublyNode<T>(item, FirstNode, null);
        if (Count > 0)
        {
            FirstNode.Prev = node;
        }
        else
        {
            LastNode = node;
        }
        FirstNode = node;
        Count++;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(First, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override void AddLast(T item)
    {
        var node = new DoublyNode<T>(item, null, LastNode);
        if (Count > 0)
        {
            LastNode.Next = node;
        }
        else
        {
            FirstNode = node;
        }
        LastNode = node;
        Count++;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(Last, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public void Append(IDoublyLinkedList<T> list)
    {
        // Preconditions
        if (list == null) throw new ArgumentNullException(nameof(list), ErrorMessages.NullList);

        if (list.Count == 0)
        {
            return;
        }
        if (list is not DoublyLinkedList<T> rll)
        {
            foreach (var i in list)
            {
                AddLast(i);
            }
            list.Clear();
            return;
        }
        if (LastNode != null)
        {
            LastNode.Next = rll.FirstNode;
            rll.FirstNode.Prev = LastNode;
        }
        else
        {
            FirstNode = rll.FirstNode;
        }
        LastNode = rll.LastNode;
        Count += rll.Count;
        list.Clear();

        // Postconditions
        Debug.Assert(list.Count == 0);
    }

    /// <inheritdoc/>
    public IEnumerator<T> GetReversedEnumerator()
    {
        DoublyNode<T> prev;
        for (var curr = LastNode; curr != null; curr = prev)
        {
            prev = curr.Prev;
            yield return curr.Item;
        }
    }

    /// <inheritdoc/>
    public override bool Remove(T item)
    {
        DoublyNode<T> node = null;
        for (var n = FirstNode; n != null; n = n.Next)
        {
            if (!EqualityComparer.Equals(n.Item, item))
            {
                continue;
            }
            node = n;
            break;
        }
        if (node == null)
        {
            // Node is not contained inside the list.
            return false;
        }

        if (node == FirstNode)
        {
            RemoveFirst();
        }
        else if (node == LastNode)
        {
            RemoveLast();
        }
        else
        {
            RemoveInnerNode(node);
        }
        return true;
    }

    /// <inheritdoc/>
    public override T RemoveFirst()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var first = FirstNode.Item;
        FirstNode = FirstNode.Next;
        if (--Count == 0)
        {
            LastNode = null;
        }
        return first;
    }

    /// <inheritdoc/>
    public T RemoveLast()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var last = LastNode.Item;
        LastNode = LastNode.Prev;
        if (--Count == 0)
        {
            FirstNode = null;
        }
        return last;
    }

    /// <inheritdoc/>
    public void Reverse()
    {
        DoublyNode<T> next;
        for (var curr = FirstNode; curr != null; curr = next)
        {
            next = curr.Next;
            curr.Next = curr.Prev;
            curr.Prev = next;
        }
        next = FirstNode;
        FirstNode = LastNode;
        LastNode = next;
    }

    #endregion IDoublyLinkedList Members

    #region Private Methods

    private void RemoveInnerNode(DoublyNode<T> node)
    {
        node.Prev.Next = node.Next;
        node.Next.Prev = node.Prev;
        Count--;
    }

    #endregion Private Methods
}
