﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the list will contain.</typeparam>
public sealed class SinglyLinkedList<T> : ListBase<SinglyNode<T>, T>, ILinkedList<T>
{
    #region Construction

    /// <summary>
    ///   Returns the default implementation of the <see cref="ILinkedList{TItem}"/> interface.
    /// </summary>
    /// <returns>An implementation of the <see cref="ILinkedList{TItem}"/> interface.</returns>
    public SinglyLinkedList()
        : base(EqualityComparer<T>.Default)
    {
    }

    /// <summary>
    ///   Returns the default implementation of the <see cref="ILinkedList{TItem}"/> interface,
    ///   using specified equality comparer.
    /// </summary>
    /// <param name="equalityComparer">
    ///   The equality comparer that it will be used to determine whether two items are equal.
    /// </param>
    /// <returns>
    ///   An implementation of the <see cref="ILinkedList{TItem}"/> interface using specified
    ///   equality comparer.
    /// </returns>
    public SinglyLinkedList(IEqualityComparer<T> equalityComparer)
        : base(equalityComparer)
    {
    }

    #endregion Construction

    #region ICollection Members

    /// <inheritdoc/>
    public override void Add(T item)
    {
        AddLast(item);
    }

    #endregion ICollection Members

    #region IThinLinkedList Members

    /// <inheritdoc/>
    public override void AddFirst(T item)
    {
        var node = new SinglyNode<T>(item, FirstNode);
        FirstNode = node;
        if (Count++ == 0)
        {
            LastNode = node;
        }

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(First, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override T RemoveFirst()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var first = FirstNode.Item;
        FirstNode = FirstNode.Next;
        if (--Count == 0)
        {
            LastNode = null;
        }
        return first;
    }

    #endregion IThinLinkedList Members

    #region ILinkedList Members

    /// <inheritdoc/>
    public override void AddLast(T item)
    {
        var node = new SinglyNode<T>(item, null);
        if (Count++ == 0)
        {
            FirstNode = node;
        }
        else
        {
            LastNode.Next = node;
        }
        LastNode = node;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(Last, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override void Append(ILinkedList<T> list)
    {
        // Preconditions
        if (list == null) throw new ArgumentNullException(nameof(list), ErrorMessages.NullList);

        if (list.Count == 0)
        {
            return;
        }
        if (list is not SinglyLinkedList<T> ll)
        {
            foreach (var i in list)
            {
                AddLast(i);
            }
            list.Clear();
            return;
        }
        if (LastNode != null)
        {
            LastNode.Next = ll.FirstNode;
        }
        else
        {
            FirstNode = ll.FirstNode;
        }
        LastNode = ll.LastNode;
        Count += ll.Count;
        list.Clear();

        // Postconditions
        Debug.Assert(list.Count == 0);
    }

    #endregion ILinkedList Members
}
