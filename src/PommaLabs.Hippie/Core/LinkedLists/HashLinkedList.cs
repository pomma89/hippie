﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the list will contain.</typeparam>
public sealed class HashLinkedList<T> : ListBase<DoublyNode<T>, T>, IHashLinkedList<T>
{
    #region Fields

    private readonly Dictionary<T, DoublyNode<T>> _nodes;

    #endregion Fields

    #region Construction

    /// <summary>
    ///   Returns the default implementation of the <see cref="IHashLinkedList{TItem}"/> interface.
    /// </summary>
    /// <returns>An implementation of the <see cref="IHashLinkedList{TItem}"/> interface.</returns>
    public HashLinkedList()
        : base(EqualityComparer<T>.Default)
    {
        _nodes = new Dictionary<T, DoublyNode<T>>(EqualityComparer<T>.Default);
    }

    /// <summary>
    ///   Returns the default implementation of the <see cref="IHashLinkedList{TItem}"/>
    ///   interface, using specified equality comparer.
    /// </summary>
    /// <param name="equalityComparer">
    ///   The equality comparer that it will be used to determine whether two items are equal.
    /// </param>
    /// <returns>
    ///   An implementation of the <see cref="IHashLinkedList{TItem}"/> interface using
    ///   specified equality comparer.
    /// </returns>
    public HashLinkedList(IEqualityComparer<T> equalityComparer)
        : base(equalityComparer)
    {
        _nodes = new Dictionary<T, DoublyNode<T>>(equalityComparer);
    }

    #endregion Construction

    #region ICollection Members

    /// <inheritdoc/>
    public override void Add(T item)
    {
        AddLast(item);
    }

    #endregion ICollection Members

    #region IHashLinkedList Members

    /// <inheritdoc/>
    public void AddAfter(T after, T toBeAdded)
    {
        // Preconditions
        if (!Contains(after)) throw new ArgumentException(ErrorMessages.NotContainedItem, nameof(after));
        if (Contains(toBeAdded)) throw new ArgumentException(ErrorMessages.ContainedItem, nameof(toBeAdded));

        if (after.Equals(LastNode.Item))
        {
            AddLast(toBeAdded);
            return;
        }
        var afterNode = _nodes[after];
        var node = new DoublyNode<T>(toBeAdded, afterNode.Next, afterNode);
        _nodes.Add(toBeAdded, node);
        afterNode.Next.Prev = node;
        afterNode.Next = node;
        Count++;

        // Postconditions
        Debug.Assert(Contains(toBeAdded));
    }

    /// <inheritdoc/>
    public void AddBefore(T before, T toBeAdded)
    {
        // Preconditions
        if (!Contains(before)) throw new ArgumentException(ErrorMessages.NotContainedItem, nameof(before));
        if (Contains(toBeAdded)) throw new ArgumentException(ErrorMessages.ContainedItem, nameof(toBeAdded));

        if (before.Equals(FirstNode.Item))
        {
            AddFirst(toBeAdded);
            return;
        }
        var beforeNode = _nodes[before];
        var node = new DoublyNode<T>(toBeAdded, beforeNode, beforeNode.Prev);
        _nodes.Add(toBeAdded, node);
        beforeNode.Prev.Next = node;
        beforeNode.Prev = node;
        Count++;

        // Postconditions
        Debug.Assert(Contains(toBeAdded));
    }

    /// <inheritdoc/>
    public override void AddFirst(T item)
    {
        // Preconditions
        if (Contains(item)) throw new ArgumentException(ErrorMessages.ContainedItem, nameof(item));

        var node = new DoublyNode<T>(item, FirstNode, null);
        _nodes.Add(item, node);

        if (Count > 0)
        {
            FirstNode.Prev = node;
        }
        else
        {
            LastNode = node;
        }
        FirstNode = node;
        Count++;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(First, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override void AddLast(T item)
    {
        // Preconditions
        if (Contains(item)) throw new ArgumentException(ErrorMessages.ContainedItem, nameof(item));

        var node = new DoublyNode<T>(item, null, LastNode);
        _nodes.Add(item, node);

        if (Count > 0)
        {
            LastNode.Next = node;
        }
        else
        {
            FirstNode = node;
        }
        LastNode = node;
        Count++;

        // Postconditions
        Debug.Assert(EqualityComparer.Equals(Last, item));
        Debug.Assert(Contains(item));
    }

    /// <inheritdoc/>
    public override void Clear()
    {
        base.Clear();
        _nodes.Clear();
    }

    /// <inheritdoc/>
    public override bool Contains(T item)
    {
        return _nodes.ContainsKey(item);
    }

    /// <inheritdoc/>
    public IEnumerator<T> GetReversedEnumerator()
    {
        DoublyNode<T> prev;
        for (var curr = LastNode; curr != null; curr = prev)
        {
            prev = curr.Prev;
            yield return curr.Item;
        }
    }

    /// <inheritdoc/>
    public override bool Remove(T item)
    {
        if (!_nodes.TryGetValue(item, out var node))
        {
            // Node is not contained inside the list.
            return false;
        }
        if (node == FirstNode)
        {
            RemoveFirst();
        }
        else if (node == LastNode)
        {
            RemoveLast();
        }
        else
        {
            RemoveInnerNode(node);
        }
        Debug.Assert(Count == _nodes.Count);
        return true;
    }

    /// <inheritdoc/>
    public T RemoveAfter(T after)
    {
        // Preconditions
        if (!Contains(after)) throw new ArgumentException(ErrorMessages.NotContainedItem, nameof(after));
        if (EqualityComparer.Equals(after, Last)) throw new InvalidOperationException();

        var afterNode = _nodes[after];
        if (ReferenceEquals(afterNode.Next, LastNode))
        {
            return RemoveLast();
        }
        var afterItem = afterNode.Next.Item;
        afterNode.Next = afterNode.Next.Next;
        afterNode.Next.Prev = afterNode;
        Count--;

        // Postconditions
        Debug.Assert(Contains(after));

        return afterItem;
    }

    /// <inheritdoc/>
    public T RemoveBefore(T before)
    {
        // Preconditions
        if (!Contains(before)) throw new ArgumentException(ErrorMessages.NotContainedItem, nameof(before));
        if (EqualityComparer.Equals(before, First)) throw new InvalidOperationException();

        var beforeNode = _nodes[before];
        if (ReferenceEquals(beforeNode.Prev, FirstNode))
        {
            return RemoveFirst();
        }
        var beforeItem = beforeNode.Prev.Item;
        beforeNode.Prev = beforeNode.Prev.Prev;
        beforeNode.Prev.Next = beforeNode;
        Count--;

        // Postconditions
        Debug.Assert(Contains(before));

        return beforeItem;
    }

    /// <inheritdoc/>
    public override T RemoveFirst()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var first = FirstNode.Item;
        _nodes.Remove(first);
        FirstNode = FirstNode.Next;
        if (--Count == 0)
        {
            LastNode = null;
        }
        return first;
    }

    /// <inheritdoc/>
    public T RemoveLast()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyList);

        var last = LastNode.Item;
        _nodes.Remove(last);
        LastNode = LastNode.Prev;
        if (--Count == 0)
        {
            FirstNode = null;
        }
        return last;
    }

    /// <inheritdoc/>
    public void Reverse()
    {
        DoublyNode<T> next;
        for (var curr = FirstNode; curr != null; curr = next)
        {
            next = curr.Next;
            curr.Next = curr.Prev;
            curr.Prev = next;
        }
        next = FirstNode;
        FirstNode = LastNode;
        LastNode = next;
    }

    #endregion IHashLinkedList Members

    #region Private Methods

    private void RemoveInnerNode(DoublyNode<T> node)
    {
        _nodes.Remove(node.Item);
        node.Prev.Next = node.Next;
        node.Next.Prev = node.Prev;
        Count--;
    }

    #endregion Private Methods
}
