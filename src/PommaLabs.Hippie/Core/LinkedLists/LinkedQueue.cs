﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using PommaLabs.Hippie.Core.LinkedLists.Core;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <typeparam name="T">The type of the items the queue will contain.</typeparam>
public sealed class LinkedQueue<T> : ILinkedQueue<T>
{
    #region Fields

    private SinglyNode<T> _firstNode;
    private SinglyNode<T> _lastNode;

    #endregion Fields

    #region Construction

    /// <summary>
    ///   Returns a queue implemented using an <see cref="ILinkedList{TItem}"/>.
    /// </summary>
    /// <returns>A queue implemented using an <see cref="ILinkedList{TItem}"/>.</returns>
    public LinkedQueue()
    {
        // Postconditions
        Debug.Assert(Count == 0);
    }

    #endregion Construction

    #region IEnumerable Members

    /// <inheritdoc/>
    public IEnumerator<T> GetEnumerator()
    {
        for (var n = _firstNode; n != null; n = n.Next)
        {
            yield return n.Item;
        }
    }

    /// <inheritdoc/>
    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    #endregion IEnumerable Members

    #region ILinkedQueue Members

    /// <inheritdoc/>
    public int Count { get; private set; }

    /// <inheritdoc/>
    public T Dequeue()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyQueue);

        var first = _firstNode.Item;
        _firstNode = _firstNode.Next;
        if (--Count == 0)
        {
            _lastNode = null;
        }
        return first;
    }

    /// <inheritdoc/>
    public void Enqueue(T item)
    {
        var node = new SinglyNode<T>(item, null);
        if (Count++ == 0)
        {
            _firstNode = node;
        }
        else
        {
            _lastNode.Next = node;
        }
        _lastNode = node;
    }

    /// <inheritdoc/>
    public T Peek()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.EmptyQueue);

        return _firstNode.Item;
    }

    #endregion ILinkedQueue Members
}
