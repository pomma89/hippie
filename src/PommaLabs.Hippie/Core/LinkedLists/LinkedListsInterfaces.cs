﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace PommaLabs.Hippie.Core.LinkedLists;

/// <summary>
/// </summary>
/// <typeparam name="T">The type of the items of the list.</typeparam>
public interface IDoublyLinkedList<T> : ILinkedList<T>
{
    /// <summary>
    ///   Appends given list to this list; after this operation, <paramref name="list"/> is
    ///   cleared, that is, it will result in an empty collection.
    /// </summary>
    /// <param name="list">The list to append to this list.</param>
    /// <exception cref="ArgumentNullException"><paramref name="list"/> is null.</exception>
    /// <remarks>
    ///   If <paramref name="list"/> was created using <see cref="DoublyLinkedList{TItem}"/>,
    ///   then this operation is done in constant time.
    /// </remarks>
    void Append(IDoublyLinkedList<T> list);

    /// <summary>
    ///   Returns an enumerator to iterate over this list in reversed order.
    /// </summary>
    /// <returns>An enumerator to iterate over this list in reversed order.</returns>
    IEnumerator<T> GetReversedEnumerator();

    /// <summary>
    ///   Removes the last item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T RemoveLast();

    /// <summary>
    ///   Reverses the contents of the list, that is, list will be ordered as the enumerator
    ///   returned by <see cref="GetReversedEnumerator"/> before calling this method.
    /// </summary>
    void Reverse();
}

/// <summary>
/// </summary>
/// <typeparam name="T">The type of the items of the list.</typeparam>
public interface IHashLinkedList<T> : ICollection<T>
{
    /// <summary>
    ///   The equality comparer used to determine whether two items are equal.
    /// </summary>
    IEqualityComparer<T> EqualityComparer { get; }

    /// <summary>
    ///   The first item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T First { get; }

    /// <summary>
    ///   The last item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T Last { get; }

    /// <summary>
    ///   Adds given item after the item specified.
    /// </summary>
    /// <param name="after">The item after which we have to put the new item.</param>
    /// <param name="toBeAdded">The item to add to the list.</param>
    /// <exception cref="ArgumentException">
    ///   List does not contain item specified by <paramref name="after"/>, or
    ///   <paramref name="toBeAdded"/> is already contained in the list.
    /// </exception>
    void AddAfter(T after, T toBeAdded);

    /// <summary>
    ///   Adds given item before the item specified.
    /// </summary>
    /// <param name="before">The item before which we have to put the new item.</param>
    /// <param name="toBeAdded">The item to add to the list.</param>
    /// <exception cref="ArgumentException">
    ///   List does not contain item specified by <paramref name="before"/>, or
    ///   <paramref name="toBeAdded"/> is already contained in the list.
    /// </exception>
    void AddBefore(T before, T toBeAdded);

    /// <summary>
    ///   Adds given item to the list, so that it is the first item of the list.
    /// </summary>
    /// <param name="item">The item to add to the list.</param>
    /// <exception cref="ArgumentException">
    ///   <paramref name="item"/> is already contained in the list.
    /// </exception>
    void AddFirst(T item);

    /// <summary>
    ///   Adds given item to the list, so that it is the last item of the list.
    /// </summary>
    /// <param name="item">The item to add to the list.</param>
    /// <exception cref="ArgumentException">
    ///   <paramref name="item"/> is already contained in the list.
    /// </exception>
    void AddLast(T item);

    /// <summary>
    ///   Returns an enumerator to iterate over this list in reversed order.
    /// </summary>
    /// <returns>An enumerator to iterate over this list in reversed order.</returns>
    IEnumerator<T> GetReversedEnumerator();

    /// <summary>
    ///   Removes the item after the item specified by <paramref name="after"/>.
    /// </summary>
    /// <param name="after">The item after which we have to apply the remove operation.</param>
    /// <exception cref="ArgumentException">List does not contain item specified by <paramref name="after"/>.</exception>
    /// <exception cref="InvalidOperationException">
    ///   <paramref name="after"/> is the last element of the list.
    /// </exception>
    T RemoveAfter(T after);

    /// <summary>
    ///   Removes the item before the item specified by <paramref name="before"/>.
    /// </summary>
    /// <param name="before">The item before which we have to apply the remove operation.</param>
    /// <exception cref="ArgumentException">List does not contain item specified by <paramref name="before"/>.</exception>
    /// <exception cref="InvalidOperationException">
    ///   <paramref name="before"/> is the first element of the list.
    /// </exception>
    T RemoveBefore(T before);

    /// <summary>
    ///   Removes the first item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T RemoveFirst();

    /// <summary>
    ///   Removes the last item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T RemoveLast();

    /// <summary>
    ///   Reverses the contents of the list, that is, list will be ordered as the enumerator
    ///   returned by <see cref="GetReversedEnumerator"/> before calling this method.
    /// </summary>
    void Reverse();
}

/// <summary>
/// </summary>
/// <typeparam name="T">The type of the items of the list.</typeparam>
public interface ILinkedList<T> : IThinLinkedList<T>
{
    /// <summary>
    ///   The last item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T Last { get; }

    /// <summary>
    ///   Adds given item to the list, so that it is the last item of the list.
    /// </summary>
    /// <param name="item">The item to add to the list.</param>
    void AddLast(T item);

    /// <summary>
    ///   Appends given list to this list; after this operation, <paramref name="list"/> is
    ///   cleared, that is, it will result in an empty collection.
    /// </summary>
    /// <param name="list">The list to append to this list.</param>
    /// <exception cref="ArgumentNullException"><paramref name="list"/> is null.</exception>
    /// <remarks>
    ///   If <paramref name="list"/> was created using <see cref="SinglyLinkedList{TItem}"/>,
    ///   then this operation is done in constant time.
    /// </remarks>
    void Append(ILinkedList<T> list);
}

/// <summary>
/// </summary>
/// <typeparam name="T">The type of the items of the queue.</typeparam>
public interface ILinkedQueue<T> : IEnumerable<T>
{
    /// <summary>
    ///   The number of items contained in the queue.
    /// </summary>
    int Count { get; }

    /// <summary>
    ///   Dequeues the first item.
    /// </summary>
    /// <exception cref="InvalidOperationException">Queue is empty.</exception>
    T Dequeue();

    /// <summary>
    ///   Enqueues given item.
    /// </summary>
    /// <param name="item">The item to add to queue.</param>
    void Enqueue(T item);

    /// <summary>
    ///   The first item of the queue.
    /// </summary>
    /// <exception cref="InvalidOperationException">Queue is empty.</exception>
    T Peek();
}

/// <summary>
/// </summary>
/// <typeparam name="T">The type of the items of the stack.</typeparam>
public interface ILinkedStack<T> : IEnumerable<T>
{
    /// <summary>
    ///   The number of items contained in the stack.
    /// </summary>
    int Count { get; }

    /// <summary>
    ///   Pops the first item off the stack.
    /// </summary>
    /// <exception cref="InvalidOperationException">Stack is empty.</exception>
    T Pop();

    /// <summary>
    ///   Pushes given item onto the stack.
    /// </summary>
    /// <param name="item">The item to push onto the stack.</param>
    void Push(T item);

    /// <summary>
    ///   The first item of the stack.
    /// </summary>
    /// <exception cref="InvalidOperationException">Stack is empty.</exception>
    T Top();
}

/// <summary>
///   Represents a linked list which has a very low memory footprint, but it also exposes very
///   few operations.
/// </summary>
/// <typeparam name="T">The type of the items of the list.</typeparam>
public interface IThinLinkedList<T> : ICollection<T>
{
    /// <summary>
    ///   The equality comparer used to determine whether two items are equal.
    /// </summary>
    IEqualityComparer<T> EqualityComparer { get; }

    /// <summary>
    ///   The first item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T First { get; }

    /// <summary>
    ///   Adds given item to the list, so that it is the first item of the list.
    /// </summary>
    /// <param name="item">The item to add to the list.</param>
    void AddFirst(T item);

    /// <summary>
    ///   Removes the first item of the list.
    /// </summary>
    /// <exception cref="InvalidOperationException">List is empty.</exception>
    T RemoveFirst();
}
