﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using PommaLabs.Hippie.Core;

namespace PommaLabs.Hippie;

public sealed class StableThinHeap<TVal, TPr> : IStableThinHeap<TVal, TPr>
{
    private readonly IThinHeap<TVal, IVersionedPriority<TPr>> _wrappedHeap;

    internal StableThinHeap(IComparer<TPr> comparer, IEqualityComparer<TVal> equalityComparer, long initialVersion)
    {
        Comparer = comparer;
        var stableComparer = new StableComparer<TPr>(comparer.Compare);
        _wrappedHeap = new ThinHeap<TVal, IVersionedPriority<TPr>>(stableComparer, equalityComparer);
        NextVersion = initialVersion;
    }

    public IComparer<TPr> Comparer { get; }

    public int Count
    {
        get { return _wrappedHeap.Count; }
    }

    public IEqualityComparer<TVal> EqualityComparer
    {
        get { return _wrappedHeap.EqualityComparer; }
    }

    public bool IsReadOnly
    {
        get { return false; }
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> Min
    {
        get { return _wrappedHeap.Min; }
    }

    IHeapHandle<TVal, TPr> IThinHeap<TVal, TPr>.Min
    {
        get { return new StableHandle<TVal, TPr>(_wrappedHeap.Min); }
    }

    public long NextVersion { get; private set; }

    public void Add(TVal value, TPr priority, long version)
    {
        _wrappedHeap.Add(value, new VersionedPriority<TPr>(priority, version));
    }

    public void Add(IHeapHandle<TVal, TPr> handle)
    {
        Add(handle.Value, handle.Priority, NextVersion++);
    }

    public void Add(TVal value, TPr priority)
    {
        Add(value, priority, NextVersion++);
    }

    public void Clear()
    {
        _wrappedHeap.Clear();
    }

    public bool Contains(IHeapHandle<TVal, TPr> item)
    {
        Debug.Assert(item is StableHandle<TVal, TPr>);
        var wrappedHandle = (item as StableHandle<TVal, TPr>)!.Handle;
        return _wrappedHeap.Any(i => ReferenceEquals(i, wrappedHandle));
    }

    public bool Contains(IHeapHandle<TVal, IVersionedPriority<TPr>> handle)
    {
        return _wrappedHeap.Any(i => ReferenceEquals(i, handle));
    }

    public void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    public IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        foreach (var handle in _wrappedHeap)
        {
            yield return new StableHandle<TVal, TPr>(handle);
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    [SuppressMessage("Major Code Smell", "S2589:Boolean expressions should not be gratuitous", Justification = "Count property in while loop is decreased by RemoveMin call")]
    public void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
        where TVal2 : TVal
        where TPr2 : TPr
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        while (other.Count > 0)
        {
            var otherMin = other.RemoveMin();
            _wrappedHeap.Add(otherMin.Value, new VersionedPriority<TPr>(otherMin.Priority, NextVersion++));
        }
    }

    public bool Remove(IHeapHandle<TVal, TPr> item)
    {
        throw new NotImplementedException(ErrorMessages.InefficientMethod);
    }

    public IHeapHandle<TVal, IVersionedPriority<TPr>> RemoveMin()
    {
        return _wrappedHeap.RemoveMin();
    }

    IHeapHandle<TVal, TPr> IThinHeap<TVal, TPr>.RemoveMin()
    {
        return new StableHandle<TVal, TPr>(_wrappedHeap.RemoveMin());
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        foreach (var t in _wrappedHeap.ToReadOnlyForest())
        {
            var tt = t.BreadthFirstVisit<ReadOnlyTree<TVal, TPr>>((tree, parent) => TransformTree(tree, parent), null);
            yield return tt.ToList()[0];
        }
    }

    private static ReadOnlyTree<TVal, TPr> TransformTree(IReadOnlyTree<TVal, IVersionedPriority<TPr>> tree, ReadOnlyTree<TVal, TPr> parent)
    {
        return new ReadOnlyTree<TVal, TPr>(tree.Value, tree.Priority.Value, parent);
    }
}

public sealed class ThinHeap<TVal, TPr> : IThinHeap<TVal, TPr>
{
    /// <summary>
    ///   The factor used to decrement the size of the array containing the handles.
    /// </summary>
    private const int DecreaseFactor = IncreaseFactor * IncreaseFactor;

    /// <summary>
    ///   The factor used to increment the size of the array containing the handles.
    /// </summary>
    private const int IncreaseFactor = 2;

    /// <summary>
    ///   The index from which handles are stored.
    /// </summary>
    private const int MinIndex = 0;

    /// <summary>
    ///   The minimum size of the array containing the handles.
    /// </summary>
    private const int MinSize = 8;

    private readonly Func<TPr, TPr, int> _comparer;

    /// <summary>
    ///   The array into which handles are stored.
    /// </summary>
    private Item[] _items;

    internal ThinHeap(IComparer<TPr> comparer, IEqualityComparer<TVal> equalityComparer)
    {
        Comparer = comparer;
        _comparer = comparer.Compare;
        EqualityComparer = equalityComparer;
        _items = new Item[MinSize];
    }

    public IComparer<TPr> Comparer { get; }

    public int Count { get; private set; }

    public IEqualityComparer<TVal> EqualityComparer { get; }

    public bool IsReadOnly
    {
        get { return false; }
    }

    public IHeapHandle<TVal, TPr> Min
    {
        get
        {
            // Preconditions
            if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

            var result = _items[MinIndex];

            // Postconditions
            Debug.Assert(Contains(result));

            return result;
        }
    }

    public void Add(TVal value, TPr priority)
    {
        Add(new Item(value, priority));
    }

    public void Add(IHeapHandle<TVal, TPr> handle)
    {
        Add(handle.Value, handle.Priority);
    }

    public void Clear()
    {
        Count = 0;
        _items = new Item[MinSize];
    }

    public bool Contains(IHeapHandle<TVal, TPr> item)
    {
        return this.Any(i => ReferenceEquals(i, item));
    }

    public void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    public IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        for (var i = MinIndex; i < Count; ++i)
        {
            yield return _items[i];
        }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    [SuppressMessage("Major Code Smell", "S2589:Boolean expressions should not be gratuitous", Justification = "Count property in while loop is decreased by RemoveMin call")]
    public void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
        where TVal2 : TVal
        where TPr2 : TPr
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        if (other is ThinHeap<TVal, TPr> otherThinHeap)
        {
            var otherItems = otherThinHeap._items;
            for (var i = MinIndex; i < otherThinHeap.Count; ++i)
            {
                Add(otherItems[i]);
            }
            otherThinHeap.Clear();
            return;
        }
        if (other is ThinHeap<TVal2, TPr2> otherThinHeap2)
        {
            var other2Items = otherThinHeap2._items;
            for (var i = MinIndex; i < otherThinHeap2.Count; ++i)
            {
                var otherItem = other2Items[i];
                Add(new Item(otherItem.Value, otherItem.Priority));
            }
            otherThinHeap2.Clear();
            return;
        }
        while (other.Count > 0)
        {
            var otherMin = other.RemoveMin();
            Add(new Item(otherMin.Value, otherMin.Priority));
        }
    }

    public bool Remove(IHeapHandle<TVal, TPr> item)
    {
        throw new NotImplementedException(ErrorMessages.InefficientMethod);
    }

    public IHeapHandle<TVal, TPr> RemoveMin()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

        var min = _items[MinIndex];
        var last = _items[--Count];

        // If we have an empty heap, we should not swap any element.
        if (Count == 0)
        {
            return min;
        }

        var idx = 0;
        int j;
        while ((j = (2 * idx) + 1) < Count)
        {
            var mc = _items[j]; // Min child
            if (j + 1 < Count && _comparer(_items[j + 1].Priority, mc.Priority) < 0)
            {
                mc = _items[++j];
            }
            if (Comparer.Compare(last.Priority, mc.Priority) <= 0)
            {
                break;
            }
            _items[idx] = mc;
            idx = j;
        }
        _items[idx] = last;

        if (Count >= MinSize && Count * DecreaseFactor == _items.Length)
        {
            Array.Resize(ref _items, Count);
        }
        CheckHandlesSize();

        return min;
    }

    public IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        if (Count == 0)
        {
            yield break;
        }
        var queue = new Queue<KeyValuePair<IndexedItem, ReadOnlyTree<TVal, TPr>>>();
        var indexedItem = new IndexedItem(_items[MinIndex], MinIndex);
        queue.Enqueue(new KeyValuePair<IndexedItem, ReadOnlyTree<TVal, TPr>>(indexedItem, (ReadOnlyTree<TVal, TPr>)null));
        ReadOnlyTree<TVal, TPr> root = null;
        while (queue.Count > 0)
        {
            var vi = queue.Dequeue();
            var it = vi.Key;
            var t = new ReadOnlyTree<TVal, TPr>(it.Value, it.Priority, vi.Value);
            if (root == null)
            {
                root = t;
            }
            var start = (2 * it.Index) + 1;
            for (var i = 0; i < 2 && i + start < Count; ++i)
            {
                var idx = start + i;
                indexedItem = new IndexedItem(_items[idx], idx);
                queue.Enqueue(new KeyValuePair<IndexedItem, ReadOnlyTree<TVal, TPr>>(indexedItem, t));
            }
        }
        yield return root;
    }

    private void Add(Item item)
    {
        // Preconditions
        if (item.Priority is null) throw new ArgumentNullException(nameof(item), ErrorMessages.NullPriority);

        if (Count == _items.Length)
        {
            Array.Resize(ref _items, Count * IncreaseFactor);
        }
        var idx = Count++;
        CheckHandlesSize();

        while (idx != MinIndex)
        {
            var j = (idx - 1) >> 1;
            var parent = _items[j];
            if (_comparer(item.Priority, parent.Priority) >= 0)
            {
                break;
            }
            _items[idx] = parent;
            idx = j;
        }
        _items[idx] = item;
    }

    [Conditional("DEBUG")]
    private void CheckHandlesSize()
    {
        Debug.Assert(DecreaseFactor == IncreaseFactor * IncreaseFactor);
        if (Count <= MinSize)
        {
            Debug.Assert(_items.Length == MinSize || _items.Length == MinSize * 2);
            return;
        }
        var expectedLog = Math.Log(_items.Length, IncreaseFactor);
        var foundLog = Math.Ceiling(Math.Log(Count, IncreaseFactor));
        Debug.Assert(expectedLog.Equals(foundLog) || expectedLog.Equals(foundLog + 1));
    }

    private sealed class IndexedItem : IHeapHandle<TVal, TPr>
    {
        public readonly int Index;
        private readonly Item _item;

        public IndexedItem(Item item, int index)
        {
            _item = item;
            Index = index;
        }

        public TPr Priority
        {
            get { return _item.Priority; }
        }

        public TVal Value
        {
            get { return _item.Value; }
        }
    }

    private sealed class Item : IHeapHandle<TVal, TPr>
    {
        public readonly TPr Priority;
        public readonly TVal Value;

        public Item(TVal value, TPr priority)
        {
            Value = value;
            Priority = priority;
        }

        TPr IHeapHandle<TVal, TPr>.Priority
        {
            get { return Priority; }
        }

        TVal IHeapHandle<TVal, TPr>.Value
        {
            get { return Value; }
        }

        public override string ToString()
        {
            return string.Format("[Value: {0}; Priority: {1}]", Value, Priority);
        }
    }
}

internal sealed class StableComparer<TPr> : IComparer<IVersionedPriority<TPr>>
{
    private readonly Func<TPr, TPr, int> _originalComparer;

    public StableComparer(Func<TPr, TPr, int> originalComparer)
    {
        _originalComparer = originalComparer;
    }

    public int Compare(IVersionedPriority<TPr> x, IVersionedPriority<TPr> y)
    {
        var comparer = _originalComparer(x.Value, y.Value);
        return (comparer != 0) ? comparer : x.Version.CompareTo(y.Version);
    }
}

internal sealed class StableHandle<TVal, TPr> : IHeapHandle<TVal, TPr>
{
    public readonly IHeapHandle<TVal, IVersionedPriority<TPr>> Handle;

    public StableHandle(IHeapHandle<TVal, IVersionedPriority<TPr>> handle)
    {
        Handle = handle;
    }

    public TPr Priority
    {
        get { return Handle.Priority.Value; }
    }

    public TVal Value
    {
        get { return Handle.Value; }
    }

    public override string ToString()
    {
        return string.Format("[Value: {0}; Priority: {1}]", Value, Priority);
    }
}

internal sealed class VersionedPriority<TPr> : IVersionedPriority<TPr>
{
    public VersionedPriority(TPr value, long version)
    {
        // Preconditions
        if (value is null) throw new ArgumentNullException(nameof(value), ErrorMessages.NullPriority);

        Value = value;
        Version = version;
    }

    public TPr Value { get; }

    public long Version { get; }

    public override string ToString()
    {
        return string.Format("[Value: {0}; Version: {1}]", Value, Version);
    }
}
