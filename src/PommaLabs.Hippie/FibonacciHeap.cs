﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using PommaLabs.Hippie.Core;
using PommaLabs.Hippie.Core.LinkedLists;

namespace PommaLabs.Hippie;

public sealed class FibonacciHeap<TVal, TPr> : TreePoolHeap<TVal, TPr>
{
    private const short EmptyMark = 0;
    private const short FullMark = 2;
    private readonly Tree[] _mergeBuffer;

    internal FibonacciHeap(IComparer<TPr> comparer)
        : base(comparer)
    {
        _mergeBuffer = new Tree[(int)Math.Ceiling(Math.Log(int.MaxValue, 2)) * 2];
    }

    public override void CopyTo(IHeapHandle<TVal, TPr>[] array, int arrayIndex)
    {
        this.CommonCopyTo(array, arrayIndex, h => h);
    }

    public override IEnumerator<IHeapHandle<TVal, TPr>> GetEnumerator()
    {
        foreach (var tp in TreePool)
        {
            foreach (var t in tp.BreadthFirstVisit())
            {
                yield return t.Handle;
            }
        }
    }

    public override void Merge<TVal2, TPr2>(IThinHeap<TVal2, TPr2> other)
    {
        // Preconditions
        if (other == null) throw new ArgumentNullException(nameof(other), ErrorMessages.NullOther);
        if (!ReferenceEquals(Comparer, other.Comparer)) throw new ArgumentException(ErrorMessages.DifferentComparers);

        if (ReferenceEquals(this, other) || other.Count == 0)
        {
            return;
        }
        if (other is not FibonacciHeap<TVal, TPr> otherFibonacciHeap)
        {
            this.CommonMerge(other);
            return;
        }
        otherFibonacciHeap.Version.Id = Version.Id; // Updates all other nodes version
        TreePool.Append(otherFibonacciHeap.TreePool);
        FixMin(otherFibonacciHeap.MinTree);
        Count += otherFibonacciHeap.Count;
        otherFibonacciHeap.Clear();
    }

    public override bool Remove(IHeapHandle<TVal, TPr> item)
    {
        var treeHandle = GetHandle(item as TreeHandle);
        if (treeHandle == null)
        {
            return false;
        }
        RemoveTree(treeHandle.Tree);
        return true;
    }

    public override IHeapHandle<TVal, TPr> RemoveMin()
    {
        // Preconditions
        if (Count == 0) throw new InvalidOperationException(ErrorMessages.NoMin);

        var min = MinTree;
        RemoveTree(min);
        return min.Handle;
    }

    public override IEnumerable<IReadOnlyTree<TVal, TPr>> ToReadOnlyForest()
    {
        foreach (var tp in TreePool)
        {
            yield return tp.ToReadOnlyTree();
        }
    }

    public override string ToString()
    {
        return this.CommonToString();
    }

    protected override void MoveDown(TreeHandle handle)
    {
        var tree = handle.Tree;
        var initialTree = tree;
        var child = tree.MinChild(Compare);
        while (child != null && Compare(child.Priority, tree.Priority) < 0)
        {
            tree.SwapRootWith(child);
            tree = child;
            child = tree.MinChild(Compare);
        }
        if (ReferenceEquals(MinTree, initialTree))
        {
            MergeSameOrderTrees();
        }
    }

    protected override void MoveUp(TreeHandle handle)
    {
        var tree = handle.Tree;
        FixMin(tree);
        var parent = tree.Parent;
        if (parent == null || Compare(parent.Priority, tree.Priority) <= 0)
        {
            return;
        }
        while (true)
        {
            parent.Children.Remove(tree);
            AppendToPool(tree);
            if (parent.Parent != null)
            {
                parent.Mark++;
            }
            if (parent.Mark != FullMark)
            {
                return;
            }
            parent.Mark = EmptyMark;
            tree = parent;
            parent = tree.Parent;
        }
    }

    private void AppendToPool(Tree tree)
    {
        tree.Parent = null;
        tree.Mark = EmptyMark;
        TreePool.AddLast(tree);
    }

    private void AppendToPool(SinglyLinkedList<Tree> trees)
    {
        foreach (var tree in trees)
        {
            tree.Parent = null;
            tree.Mark = EmptyMark;
        }
        TreePool.Append(trees);
    }

    private protected override void FixMin(Tree tree)
    {
        if (MinTree == null || Compare(tree.Priority, MinTree.Priority) < 0)
        {
            MinTree = tree;
        }
    }

    private void MergeSameOrderTrees()
    {
        if (TreePool.Count <= 1)
        {
            if (TreePool.Count == 0)
            {
                MinTree = null;
                return;
            }
            MinTree = TreePool.First;
            return;
        }

        var maxIndex = 0;
        foreach (var tree in TreePool)
        {
            var merged = tree;
            var mergedIndex = merged.Children.Count;
            Tree aux;
            while ((aux = _mergeBuffer[mergedIndex]) != null)
            {
                merged = Meld(merged, aux);
                _mergeBuffer[mergedIndex] = null;
                mergedIndex = merged.Children.Count;
            }
            _mergeBuffer[mergedIndex] = merged;
            if (mergedIndex > maxIndex)
            {
                maxIndex = mergedIndex;
            }
        }

        TreePool.Clear();
        MinTree = null;
        for (var i = 0; i <= maxIndex; ++i)
        {
            var tree = _mergeBuffer[i];
            if (tree == null)
            {
                continue;
            }
            TreePool.AddLast(tree);
            FixMin(tree);
            _mergeBuffer[i] = null;
        }
    }

    private void RemoveTree(Tree tree)
    {
        for (var p = tree.Parent; p != null; tree = p, p = p.Parent)
        {
            tree.SwapRootWith(p);
        }
        TreePool.Remove(tree);
        AppendToPool(tree.Children);
        if (ReferenceEquals(tree, MinTree))
        {
            MergeSameOrderTrees();
        }
        Count--;
        tree.Handle.Version = null;
    }
}
